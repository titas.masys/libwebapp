package com.example.demo;

import com.example.demo.doa.LibraryFunctions;
import com.example.demo.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class WebApplicationTests {

	@Test
	void contextLoads()
	{

		LibraryFunctions test = new LibraryFunctions("testlib.json","testperson.lib");
		testCreate(test);
		testGet(test);
		testTake(test);
		testShow(test);
		testDelete(test);

	}

    void testDelete(LibraryFunctions test)
	{
		test.deleteBook("84b1800e-49a8-4bce-bbc2-b4542f3335c0");
	}

	void testCreate(LibraryFunctions test)
	{
		test.createBook("Republic","Plato","Philosophy","English","1968-07-12","ISBN 3-598-21500-2","b6a9780c-0d09-4b2c-9f17-1a0bfa553525");
	}

	void testGet(LibraryFunctions test)
	{
		Book book = test.getBookByGuid("b6a9780c-0d09-4b2c-9f17-1a0bfa553525");
	}

	void testTake(LibraryFunctions test)
	{
		test.takeBook("Julius", "Pagirys","1989999","b6a9780c-0d09-4b2c-9f17-1a0bfa553525",2);
	}

	void testShow(LibraryFunctions test)
	{
		List<Book> book2 = test.showBooks(null,null,null,null,null,true);
	}
}
