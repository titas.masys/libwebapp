package com.example.demo.doa;
import com.example.demo.model.Book;
import com.example.demo.model.Person;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Repository("functions")
public class LibraryFunctions implements LibraryDoa
{
    //Tells how many books are the maximum ammount
    private final int bookMaximum = 3;
    private String libraryPath;
    private String personPath;

    public LibraryFunctions(String libraryPath, String personPath)
    {
        if(libraryPath != null && personPath != null)
        {
            this.libraryPath = libraryPath;
            this.personPath = personPath;
        }

    }
    public LibraryFunctions()
    {
        this.libraryPath = "library.json";
        this.personPath = "person.json";
    }


    //Creates and adds a book to the json library. name - the name of the book, author - the author, category - the genre of the book, pubDate - publication date,
    // isbn - the isbn of the book, guid - globally unique identifier.
    @Override
    public void createBook(String name, String author, String category, String language, String pubDate, String isbn, String guid)
    {
        try
        {
                File testIfCreated = new File("library.json");
                JSONObject libObj = null;
                if(testIfCreated.exists())
                {
                    libObj = loadJsonFromFile("library.json");
                }
                if(libObj != null && libObj.has(guid))
                {
                    throw new Exception("Book exists.");
                }
                FileOutputStream dir = new FileOutputStream("library.json");
                JSONObject bookObj = new JSONObject();
                Book book = new Book(name, author, category, language, pubDate, isbn, guid);
                JSONObject obj = new JSONObject();
                obj.put("Name",book.getName());
                obj.put("Author",book.getAuthor());
                obj.put("Category",book.getCategory());
                obj.put("Language",book.getLanguage());
                obj.put("Publication_date",book.getPubDate());
                obj.put("ISBN",book.getIsbn());

                //If there are books in the library, then it adds the book to the library object.
                if(libObj != null)
                {
                    libObj.put(book.getGuid(), obj);
                    dir.write(libObj.toString().getBytes());
                }
                //If the library is empty, then this book is the library object.
                else
                {
                    bookObj.put(book.getGuid(), obj);
                    dir.write(bookObj.toString().getBytes());
                }
                dir.close();


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    //Returns a json object from file, path - the file location path.
    private JSONObject loadJsonFromFile(String path)
    {
        try
        {
            BufferedReader input = new BufferedReader(new FileReader(path));
            String line;
            StringBuilder text = new StringBuilder();
            while((line = input.readLine()) != null)
            {
                text.append(line);
            }
            return new JSONObject(text.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    //Creates a new person json file where information about what books are stored.
    @Override
    public void takeBook(String name, String surname, String cardId, String guid, int months)
    {
        try
        {
            //Checks to see if library exists
            File testIfCreated = new File(libraryPath);
            if(!testIfCreated.exists())
            {
                throw new FileNotFoundException("Library does not exist.");
            }

            JSONObject libObj = loadJsonFromFile(libraryPath);
            JSONObject perObj = new JSONObject();
            testIfCreated = new File(personPath);
            //Checks to see if person list exists.
            if (testIfCreated.exists())
            {
                perObj = loadJsonFromFile(personPath);
            }
            //Checks if takeout period is lesser than 2 months
            if(months <= (bookMaximum-1) && months > 0)
            {
                if (libObj.has(guid))
                {
                    //Check if book isn't taken, if it is then throw exception.
                    for(int i = 0; i < perObj.length(); i++)
                    {
                        int temp = perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").length();
                        for(int j = 0; j < temp; j++)
                        {
                            String guidTemp = perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").getJSONObject(j).getString("Book_Guid");
                            if(guid.equals(guidTemp))
                            {
                                throw new Exception("Book already taken.");
                            }
                        }
                    }
                    Person person = new Person(name, surname, cardId);
                    //Check if a person exists.
                    if (perObj.has(cardId))
                    {
                        if(perObj.getJSONObject(cardId).get("Name").equals(person.getName()) && perObj.getJSONObject(cardId).get("Surname").equals(person.getSurname()))
                        {
                            //Then add a new book to the persons array the person hasn't got more than 3 books.
                            if (perObj.getJSONObject(person.getCardId()).getJSONArray("Books").length() < 3)
                            {
                                FileOutputStream dir = new FileOutputStream(personPath);
                                JSONObject termData = new JSONObject();
                                termData.put("Months", months);
                                termData.put("Book_Guid", guid);
                                perObj.getJSONObject(person.getCardId()).getJSONArray("Books").put(termData);
                                dir.write(perObj.toString().getBytes());
                                dir.close();
                            }
                            //Throw exception otherwise.
                            else
                            {
                                throw new ArrayIndexOutOfBoundsException("Person has maximum amount of books.");
                            }
                        }
                        else
                        {
                            throw new Exception("Persons name and surname does not match the card id.");
                        }
                    }
                    //If the person does not exist create a new one.
                    else
                    {
                        FileOutputStream dir = new FileOutputStream(personPath);
                        JSONObject obj = new JSONObject();
                        JSONArray term = new JSONArray();
                        JSONObject termData = new JSONObject();
                        obj.put("Name", person.getName());
                        obj.put("Surname", person.getSurname());
                        termData.put("Months", months);
                        termData.put("Book_Guid", guid);
                        term.put(termData);
                        obj.put("Books", term);
                        perObj.put(person.getCardId(), obj);
                        dir.write(perObj.toString().getBytes());
                        dir.close();
                    }

                }
                else
                {
                    throw new Exception("Book does not exist.");
                }
            }
            else
            {
                throw new Exception("Book takeout period is more than 2 months.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    //Gets a book by the guid.
    @Override
    public Book getBookByGuid(String guid)
    {
        try
        {
            //Checks to see if library exists
            File testIfCreated = new File(libraryPath);
            if(!testIfCreated.exists())
            {
                throw new FileNotFoundException("Library does not exist.");
            }
            //Looks for the book with the guid, if found returns the book.
            JSONObject libObj = loadJsonFromFile(libraryPath);
            if (libObj.has(guid))
            {
                Book book = new Book();
                book.setName(libObj.getJSONObject(guid).get("Name").toString());
                book.setAuthor(libObj.getJSONObject(guid).get("Author").toString());
                book.setCategory(libObj.getJSONObject(guid).get("Category").toString());
                book.setLanguage(libObj.getJSONObject(guid).get("Language").toString());
                book.setPubDate(libObj.getJSONObject(guid).get("Publication_date").toString());
                book.setIsbn(libObj.getJSONObject(guid).get("ISBN").toString());
                return book;
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Shows books by specified filters, all of them could be used or none. None - null;
    @Override
    public List<Book> showBooks(String nameFilter, String authorFilter, String categoryFilter, String languageFilter, String isbnFilter, Boolean taken)
    {
        try
        {
            //Checks to see if library exists
            File testIfCreated = new File(libraryPath);
            if(!testIfCreated.exists())
            {
                throw new FileNotFoundException("Library does not exist.");
            }
            JSONObject libObj = loadJsonFromFile(libraryPath);

            //Puts all the books into a list.
            List<Book> books = new ArrayList<>(libObj.length());
            for(int i = 0; i < libObj.length(); i++)
            {
                Book temp = new Book();
                temp.setName(libObj.getJSONObject(libObj.names().get(i).toString()).get("Name").toString());
                temp.setAuthor(libObj.getJSONObject(libObj.names().get(i).toString()).get("Author").toString());
                temp.setCategory(libObj.getJSONObject(libObj.names().get(i).toString()).get("Category").toString());
                temp.setLanguage(libObj.getJSONObject(libObj.names().get(i).toString()).get("Language").toString());
                temp.setPubDate(libObj.getJSONObject(libObj.names().get(i).toString()).get("Publication_date").toString());
                temp.setIsbn(libObj.getJSONObject(libObj.names().get(i).toString()).get("ISBN").toString());
                temp.setGuid(libObj.names().get(i).toString());
                books.add(temp);
            }

            //This arraylist holds every filtered book by certain filter, the first member of this array
            // is always all of the books in the library.
            ArrayList<List<Book>> result = new ArrayList<>();
            result.add(books);

            JSONObject perObj = null;
            testIfCreated = new File(personPath);
            //Checks to see if person list exists.
            if (testIfCreated.exists())
            {
                perObj = loadJsonFromFile(personPath);
            }

            //Filtering data.
            //Filters books that are taken or not.
            if(perObj != null && taken != null)
            {
                List<String> guidList = new ArrayList<>();
                for (int i = 0; i < perObj.length(); i++)
                {
                    int temp = perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").length();
                    for(int j = 0; j < temp; j++)
                    {
                        guidList.add(perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").getJSONObject(j).getString("Book_Guid"));
                    }
                }
                List<Book> temp = new ArrayList<>();
                for (String s : guidList)
                {
                    for (Book book: books)
                    {
                        if (s.equals(book.getGuid()))
                        {
                            temp.add(book);
                        }
                    }
                }
                if (!taken)
                {
                    temp.removeAll(books);
                }
                result.add(temp);
            }

            //Name filter
            if(nameFilter != null)
            {
                List<Book> temp = new ArrayList<>();
                for (Book book : books) {
                    if (book.getName().equals(nameFilter))
                    {
                       temp.add(book);
                    }
                }
                result.add(temp);
            }

            //Author filter
            if(authorFilter != null)
            {
                List<Book> temp = new ArrayList<>();
                for (Book book : books) {
                    if (book.getAuthor().equals(authorFilter))
                    {
                        temp.add(book);
                    }
                }
                result.add(temp);
            }

            //Category filter
            if(categoryFilter != null)
            {
                List<Book> temp = new ArrayList<>();
                for (Book book : books) {
                    if (book.getCategory().equals(categoryFilter))
                    {
                        temp.add(book);
                    }
                }
                result.add(temp);
            }

            //Language filter
            if(languageFilter != null)
            {
                List<Book> temp = new ArrayList<>();
                for (Book book : books) {
                    if (book.getLanguage().equals(languageFilter))
                    {
                        temp.add(book);
                    }
                }
                result.add(temp);
            }

            //Isbn filter
            if(isbnFilter != null)
            {
                List<Book> temp = new ArrayList<>();
                for (Book book : books) {
                    if (book.getIsbn().equals(isbnFilter))
                    {
                        temp.add(book);
                    }
                }
                result.add(temp);
            }

            //Intersect each list to first member.
            for(int i = 1; i < result.size(); i++)
            {
                result.get(0).retainAll(result.get(i));
            }

            //Return the result of intersection.
            if(!result.get(0).isEmpty())
            {
                return result.get(0);
            }
            //If no members were filtered return null.
            else
                return null;

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Deletes a book from the library and from people who have taken them.
    @Override
    public void deleteBook(String guid)
    {
        try
        {
            File testIfCreated = new File(libraryPath);
            JSONObject libObj = new JSONObject();
            if(testIfCreated.exists())
            {
                libObj = loadJsonFromFile(libraryPath);
            }
            else
            {
                throw new FileNotFoundException("Library does not exist.");
            }

            if(libObj.has(guid))
            {
                FileOutputStream dir = new FileOutputStream(libraryPath);
                libObj.remove(guid);
                dir.write(libObj.toString().getBytes());
                testIfCreated = new File(personPath);
                //Checks to see if person list exists.
                if (testIfCreated.exists())
                {
                    JSONObject perObj = new JSONObject();
                    perObj = loadJsonFromFile(personPath);
                    FileOutputStream dirNew = new FileOutputStream(personPath);
                    for(int i = 0; i < perObj.length(); i++)
                    {

                        int tempLen = perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").length();

                        for(int j = 0; j < tempLen; j++)
                        {
                            String temp = perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").getJSONObject(j).getString("Book_Guid");
                            if (temp.equals(guid))
                            {
                                perObj.getJSONObject(perObj.names().get(i).toString()).getJSONArray("Books").remove(j);
                            }
                        }
                    }
                    dirNew.write(perObj.toString().getBytes());
                    dirNew.close();
                }
                dir.close();
            }
            else
            {
                throw new Exception("Book does not exist.");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    //Puts pre-written books for testing.
    public void addPrewrittenBooks()
    {
        createBook("Republic","Plato","Philosophy","English","1968-07-12","ISBN 3-598-21500-2","b6a9780c-0d09-4b2c-9f17-1a0bfa553525");
        createBook("Fragments","Heraclitus","Philosophy","English","1975-10-20","ISBN 3-598-21549-5","7d93f0ae-764e-4078-9c13-77bee59d5eaa");
        createBook("Tataja","Titas Kavaliauskas","History","Lithuanian","2000-01-12","ISBN 3-678-21500-4","b6a9780c-0f09-4b2c-9f17-1a0cfa553525");
        createBook("Le Petit Prince","Antoine de Saint-ExupĆ©ry","Literature for kids","French","1999-12-12","ISBN 3-598-21512-6","84b1800e-49a8-4bce-bbc2-b4542f3335c0");
    }
}
