package com.example.demo.doa;

import com.example.demo.model.Book;

import java.util.List;

public interface LibraryDoa
{
    public void createBook(String name, String author, String category, String language, String pubDate, String isbn, String guid);

    public void takeBook(String name, String surname, String cardId, String guid, int months);

    public Book getBookByGuid(String guid);

    public List<Book> showBooks(String nameFilter, String authorFilter, String categoryFilter, String languageFilter, String isbnFilter, Boolean taken);

    public void deleteBook(String guid);
}
