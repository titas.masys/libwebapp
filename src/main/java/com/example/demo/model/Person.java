package com.example.demo.model;

public class Person
{
    private final String name;
    private final String surname;
    private final String cardId;

    public Person(String name, String surname, String cardId)
    {
        this.name = name;
        this.surname = surname;
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCardId() {
        return cardId;
    }

}
