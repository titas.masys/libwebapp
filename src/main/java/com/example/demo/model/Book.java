package com.example.demo.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class Book
{
    private String name = "";
    private String author = "";
    private String category = "";
    private String language = "";
    private String pubDate = "";
    private String isbn = "";
    private String guid = "";

    public Book(String name, String author, String category, String language, String pubDate, String isbn, String guid)
    {
        setName(name);
        setAuthor(author);
        setCategory(category);
        setLanguage(language);
        setPubDate(pubDate);
        setIsbn(isbn);
        setGuid(guid);
    }
    public Book()
    {

    }

    public String getName()
    {
        return name;
    }

    public String getAuthor()
    {
        return author;
    }

    public String getCategory()
    {
        return category;
    }

    public String getLanguage()
    {
        return language;
    }

    public String getPubDate()
    {
        return pubDate;
    }

    public String getIsbn()
    {
        return isbn;
    }

    public String getGuid()
    {
        return guid;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setPubDate(String pubDate)
    {
        try
        {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pubDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        this.pubDate = pubDate;
    }

    public void setIsbn(String isbn)
    {
        try
        {
            String regex = "^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$";
            Pattern pattern = Pattern.compile(regex);
            if(pattern.matcher(isbn).matches())
            {
                this.isbn = isbn;
            }
            else
            {
                throw new Exception("ISBN is not in right format");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    public void setGuid(String guid)
    {
        try
        {
            String regex = "^[{]?[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}[}]?$";
            Pattern pattern = Pattern.compile(regex);
            if(pattern.matcher(guid).matches())
            {
                this.guid = guid;
            }
            else
            {
                throw new Exception("Guid is not in right format");
            }
         }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    public void printBook()
    {
        System.out.println(name);
        System.out.println(author);
        System.out.println(category);
        System.out.println(pubDate);
        System.out.println(language);
        System.out.println(isbn);
        System.out.println(guid);

    }
}
