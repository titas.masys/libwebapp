package com.example.demo.service;

import com.example.demo.doa.LibraryDoa;
import com.example.demo.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryService
{
    private final LibraryDoa library;

    @Autowired
    public LibraryService(@Qualifier("functions") LibraryDoa library) {
        this.library = library;
    }

    public void createBook(String name, String author, String category, String language, String pubDate, String isbn, String guid)
    {
        library.createBook(name,author,category,language,pubDate,isbn,guid);
    }

    public void takeBook(String name, String surname, String cardId, String guid, int months)
    {
        library.takeBook(name,surname,cardId,guid,months);
    }

    public Book getBookByGuid(String guid)
    {
        return library.getBookByGuid(guid);
    }

    public List<Book> showBooks(String nameFilter, String authorFilter, String categoryFilter, String languageFilter, String isbnFilter, Boolean taken)
    {
        return library.showBooks(nameFilter,authorFilter,categoryFilter,languageFilter,isbnFilter,taken);
    }

    public void deleteBook(String guid)
    {
        library.deleteBook(guid);
    }
}
