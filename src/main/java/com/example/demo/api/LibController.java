package com.example.demo.api;
import com.example.demo.model.Book;
import com.example.demo.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LibController
{
    private final LibraryService service;

    @Autowired
    public LibController(LibraryService service)
    {
        this.service = service;
    }

    @PostMapping("/createBook")
    @ResponseBody
    public void createBook(@RequestParam String name,@RequestParam String author,@RequestParam String category, @RequestParam String language,
                           @RequestParam String pubDate,@RequestParam String isbn, @RequestParam String guid)
    {
        service.createBook(name,author,category,language,pubDate,isbn,guid);
    }

    @PostMapping("/takeBook")
    @ResponseBody
    public void takeBook(@RequestParam String name, @RequestParam String surname, @RequestParam String cardId,
                         @RequestParam String guid, @RequestParam int months)
    {
        service.takeBook(name,surname,cardId,guid,months);
    }

    @GetMapping("/getBookByGuid")
    @ResponseBody
    public Book getBookByGuid(@RequestParam String guid)
    {
        return service.getBookByGuid(guid);
    }

    @GetMapping("/showBooks")
    @ResponseBody
    public List<Book> showBooks(@RequestParam(required = false) String nameFilter,@RequestParam(required = false) String authorFilter, @RequestParam(required = false) String categoryFilter,
                                @RequestParam(required = false) String languageFilter, @RequestParam(required = false) String isbnFilter, @RequestParam(required = false) Boolean taken)
    {
        return service.showBooks(nameFilter,authorFilter,categoryFilter,languageFilter,isbnFilter,taken);
    }

    @DeleteMapping("/delete")
    @ResponseBody
    public void deleteBook(@RequestParam String guid)
    {
        service.deleteBook(guid);
    }
}
